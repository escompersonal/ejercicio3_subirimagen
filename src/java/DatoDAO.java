
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carlos
 */
public class DatoDAO {

    private static final String SQL_SELECT = "select * from datos where id=?";
    private static final String SQL_INSERT = "insert into datos (nombre,paterno,materno,foto) values (?, ?, ?, ?)";

    private String dbUrl = "jdbc:mysql://localhost:3306/demo";
    private String usuario = "root", clave = "root";
    private Connection con = null;

    private void obtenerConexion() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(dbUrl, usuario, clave);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cerrarConexion() {
        if (con != null) {
            try {
                con.close();
                System.out.println("Conexion cerrada");
            } catch (SQLException e) {
                System.out.println("No se pudo cerrar la conexion");
                e.printStackTrace();
            }

        }
    }

    public String create(Dato d) {
        String mensaje = "";
        obtenerConexion();
        try {

            PreparedStatement ps = con.prepareStatement("insert into datos (nombre,paterno,materno,foto) values (?, ?, ?, ?)");
            ps.setString(1, d.getNombre());
            ps.setString(2, d.getPaterno());
            ps.setString(3, d.getMaterno());
            ps.setBlob(4, d.getFoto());

            int filasInsertadas = ps.executeUpdate();
            if (filasInsertadas > 0) {
                mensaje = "Registro OK";
            }

        } catch (SQLException e) {
            e.printStackTrace();
            mensaje = "Error al insertar";
        }
        return mensaje;
    }

    public Dato select(int id) {
        try {
            PreparedStatement ps = null;
            ResultSet rs = null;
            ArrayList listaEventos = new ArrayList();
            obtenerConexion();
            ps = con.prepareStatement(SQL_SELECT);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            Blob img;
            if (rs != null) {
                Dato d = new Dato();
                d.setNombre(rs.getString(2));
                d.setPaterno(rs.getString(3));
                d.setMaterno(rs.getString(4));
                img = rs.getBlob(5);
                d.setFoto(img.getBinaryStream(1,img.length()));               
                return d;
            } else {
                cerrarConexion();
                return null;
            }
        } catch (SQLException ex) {
            return null;
        }        
    }

}
