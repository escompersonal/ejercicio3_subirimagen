/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author alumno
 */
@WebServlet(urlPatterns = {"/uploadEnBdServlet"})
@MultipartConfig(maxFileSize = 16000000)
public class FileUploadEnBdServlet extends HttpServlet {

    private String dbUrl = "jdbc:mysql://localhost:3306/demo";
    private String usuario = "root", clave = "root";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession sesion = request.getSession(true);
            String nombre = request.getParameter("txtNombre");
            String paterno = request.getParameter("txtPaterno");
            String materno = request.getParameter("txtMaterno");

            InputStream is = null;
            String mensaje = "";
            Part archivo = request.getPart("txtFoto");

            if (archivo != null) {
                Dato d = new Dato();
                d.setNombre(nombre);
                d.setPaterno(paterno);
                d.setMaterno(materno);
                d.setFoto(archivo.getInputStream());
                sesion.setAttribute("foto", archivo.getInputStream());
                sesion.setAttribute("contentType", archivo.getContentType());
                Integer length = (int) archivo.getSize();
                sesion.setAttribute("length", length);
                DatoDAO dao = new DatoDAO();
                mensaje = dao.create(d);

            }

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FileUploadEnBdServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Status: " + mensaje + "</h1>");
            out.println("<p>Nombre: " + nombre + "      Paterno: " + paterno + "        Materno: " + materno + "        Foto: <image src=\"MostrarImagen\"/></p>");
            out.println("</body>");
            out.println("</html>");

        }
    }

    //mostrar todos lo datos almacenados
    //habilitar capacidad de Mysql para almacenar archivos mayores a 2 MB
    //Crear la pagina (servlet) que muestre una salidad como:
    //datos alamcenados en BD
    //nombre:asd dasd sad
    //Archivo foto: imagen.png
    //lo mismo con fileuploader
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
